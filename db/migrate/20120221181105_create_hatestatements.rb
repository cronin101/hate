class CreateHatestatements < ActiveRecord::Migration
  def change
    create_table :hatestatements do |t|
      t.string :statement
      t.string :submitter
      t.boolean :profanity

      t.timestamps
    end
  end
end
