class Hatestatement < ActiveRecord::Base
  attr_accessible :statement, :profanity

  validates_presence_of :statement
end
