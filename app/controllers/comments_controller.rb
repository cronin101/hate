class CommentsController < ApplicationController
  def create
    @comment = Comment.new(params[:comment])
    if current_user
      @comment.submitter = current_user.name
    else
      @comment.submitter = "Anonymous"
    end
    @comment.save
    redirect_to :back
  end

end
