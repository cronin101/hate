class HatestatementsController < ApplicationController
 
  def new
    @hates = Hatestatement.find(:all)
    @post = Hatestatement.new
  end

  def showhate
    @hates = Hatestatement.find(:all)
  end

  def create
    @post = Hatestatement.new(params[:hatestatement])
    if current_user
      @post.submitter = current_user.name
    else
      @post.submitter = "notloggedin"
    end
    @post.profanity = false
    
    ["shit","fuck","cunt","piss","nigger","rape"].each do |p|
      if @post.statement.downcase.include? p
        @post.profanity = true
        break
      end
    end

    @post.ip = request.remote_ip()

    if @post.save
      redirect_to root_url, :notice => "Submitted!"
    else
      redirect_to root_url, :notice => "Failed!"
    end
  end
  
  def singlehate
    @comment = Comment.new
    @comments = Comment.find_all_by_statementid(params[:id])
    @hates = Hatestatement.find(:all)
      @wordhash = Hash.new
      @bigrams = Hash.new
      @trigrams = Hash.new
      @hates.each do |h|
        state = h.statement.split(" ")
        state.each_with_index do |s,i|
          if i < state.size - 1
            c = s.downcase.gsub(/[^0-9a-z]/i,'')
            n = state[i+1].downcase.gsub(/[^0-9a-z]/i,'')
            @bigrams[[c,n]] = (@bigrams[[c,n]] ||= 0) + 1
          end
          if i < state.size - 2
            nn = state[i+2].downcase.gsub(/[^0-9a-z]/i,'')
            @trigrams[[c,n,nn]] = (@trigrams[[c,n,nn]] ||= 0) +1 
          end
        end
      end
      @hates.each do |h|
        if h
          h.statement.split(" ").each do |w|
            word = w.downcase.gsub("'s","").gsub(/[^0-9a-z]/i,'')
            @wordhash[word] = (@wordhash[word] ||= 0)+1
          end
        end
      end
      @stopwords = File.open("/ruby/kungfu/ruby-hax/common.txt").read.split
      @stopwords.each{|s| s.gsub!(/\d/,''); @wordhash[s] = 0; @wordhash.delete(s)}
      @hate = Hatestatement.find_by_id(params[:id])
      @topthemes = []
      @wordhash.sort{|b,a| a[1] <=> b[1]}[0..(@wordhash.size/4)-1].each{|e| @topthemes << e[0]}
      @themes = @hate.statement.split(" ").map(&:downcase).map{|s| s.gsub("'s",""); s.gsub("'","");} & @topthemes
  end

  def hatestat
      @hates = Hatestatement.find(:all)
      @wordhash = Hash.new
      @bigrams = Hash.new
      @trigrams = Hash.new
      @hates.each do |h|
        state = h.statement.split(" ")
        state.each_with_index do |s,i|
          if i < state.size - 1
            c = s.downcase.gsub(/[^0-9a-z]/i,'')
            n = state[i+1].downcase.gsub(/[^0-9a-z]/i,'')
            @bigrams[[c,n]] = (@bigrams[[c,n]] ||= 0) + 1
          end
          if i < state.size - 2
            nn = state[i+2].downcase.gsub(/[^0-9a-z]/i,'')
            @trigrams[[c,n,nn]] = (@trigrams[[c,n,nn]] ||= 0) +1 
          end
        end
      end
      @hates.each do |h|
        if h
          h.statement.split(" ").each do |w|
            word = w.downcase.gsub(/[^0-9a-z]/i,'')
            @wordhash[word] = (@wordhash[word] ||= 0)+1
          end
        end
      end
      @stopwords = File.open("/ruby/kungfu/ruby-hax/common.txt").read.split
      @stopwords.each{|s| s.gsub!(/\d/,''); @wordhash[s] = 0; @wordhash.delete(s)}
  end

end
