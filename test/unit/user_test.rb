require 'test_helper'

class UserTest < ActiveSupport::TestCase
    test "user attributes must not be empty" do
        user = User.new
        assert user.invalid?
        assert user.errors[:name].any?
        assert user.errors[:email].any?
        assert user.errors[:password].any?
        #password confirmation is not tested as by trasnitivity it is tested by
        #the present of password
    end

    test "user name must be unique" do
        alice = User.new(name: "alice",
                         email: "alice@alice.org",
                         password: "alice01",
                         password_confirmation: "alice01")

        alice.save

        bob = User.new(name: "alice",
                         email: "alice@alice.org",
                         password: "alice01",
                         password_confirmation: "alice01")

        assert !bob.save
    end

    test "email must be unique" do

        alice = User.new(name: "alice",
                         email: "alice@alice.org",
                         password: "alice01",
                         password_confirmation: "alice01")

        alice.save

        bob = User.new(name: "alice",
                         email: "alice@alice.org",
                         password: "alice01",
                         password_confirmation: "alice01")

        assert !bob.save
    end

    test "password and password confirmation must match" do

        alice = User.new(name: "alice",
                         email: "alice@alice.org",
                         password: "alice01",
                         password_confirmation: "bob")

        !alice.save
    end



end


